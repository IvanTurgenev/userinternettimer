module gitlab.com/IvanTurgenev/userinternettimer

go 1.16

require (
	github.com/akamensky/argparse v1.3.1
	github.com/cheggaaa/go-exit v1.0.5
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
)
