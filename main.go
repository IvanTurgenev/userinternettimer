package main

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"time"

	"github.com/akamensky/argparse"

	"github.com/cheggaaa/go-exit"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type firewalldCycle struct {
	add    bool
	remove bool
}

const dataFolder = "/var/lib/userinternettimer/"

var sudoCommandString = "sudo"

var addCommands [][]string
var removeCommands [][]string

var timeLimit int

var firewalldCommandStringDropArguments = []string{
	"firewall-cmd", "--direct", "--permanent", "--add-rule", "ipv4", "filter",
	"2", "-j", "DROP",
}

var firewalldCommandStringOwnerArguments = []string{
	"firewall-cmd", "--direct", "--permanent", "--add-rule", "ipv4", "filter",
	"OUTPUT", "1", "-m", "owner", "--uid-owner", "-j",
}
var firewalldCommandStringChainArguments = []string{
	"firewall-cmd", "--direct", "--permanent", "--add-chain", "ipv4", "filter",
}

var firewalldReloadCommand = []string{
	"firewall-cmd", "--reload",
}

func putStringAfterArgument(afterString string, afterArgument string, arguments []string) ([]string, error) {
	for i, argument := range arguments {
		if argument == afterArgument {
			return append(arguments[:i+1], append([]string{afterString}, arguments[i+1:]...)...), nil
		}

	}
	return nil, errors.New("Could not find after argument: " + afterArgument)

}

func replaceArgumentWithString(stringReplacement string, argumentToReplace string, arguments []string) ([]string, error) {
	for i, argument := range arguments {
		if argument == argumentToReplace {
			return append(arguments[:i], append([]string{stringReplacement}, arguments[i+1:]...)...), nil
		}

	}
	return nil, errors.New("Could not find after argument: " + argumentToReplace)

}

func insertUsernameOwner(userName string, chainString []string, cycle firewalldCycle) []string {
	if cycle.add {
		tempChainString, err := putStringAfterArgument(userName, "--uid-owner", chainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}
		tempChainString, err = putStringAfterArgument("restrict_"+userName, "-j", tempChainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

		return tempChainString

	}
	if cycle.remove {
		tempChainString, err := putStringAfterArgument(userName, "--uid-owner", chainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}
		tempChainString, err = putStringAfterArgument("restrict_"+userName, "-j", tempChainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}
		tempChainString, err = replaceArgumentWithString("--remove-rule", "--add-rule", tempChainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

		return tempChainString
	}
	log.Info("Bad cycle: ", cycle)
	os.Exit(1)
	return nil

}

func insertUsernameChain(userName string, chainString []string, cycle firewalldCycle) []string {
	if cycle.add {
		tempChainString, err := putStringAfterArgument("restrict_"+userName, "filter", chainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

		return tempChainString

	}
	if cycle.remove {
		tempChainString, err := putStringAfterArgument("restrict_"+userName, "filter", chainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

		tempChainString, err = replaceArgumentWithString("--remove-chain", "--add-chain", tempChainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

		return tempChainString
	}
	log.Info("Bad cycle: ", cycle)
	os.Exit(1)
	return nil

}

func insertUsernameDrop(userName string, chainString []string, cycle firewalldCycle) []string {
	if cycle.add {
		tempChainString, err := putStringAfterArgument("restrict_"+userName, "filter", chainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

		return tempChainString

	}

	if cycle.remove {
		tempChainString, err := putStringAfterArgument("restrict_"+userName, "filter", chainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}
		tempChainString, err = replaceArgumentWithString("--remove-rule", "--add-rule", tempChainString)
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

		return tempChainString
	}
	log.Info("Bad cycle: ", cycle)
	os.Exit(1)
	return nil

}

func options() {
	// Create new parser object
	parser := argparse.NewParser("userInternetTimer", "Disconnects a linux user internet connection by time usage, using firewalld")
	// Create string flag
	userName := parser.String("u", "user", &argparse.Options{Required: true, Help: "User name of user to block"})
	tempTimeLimit := parser.Int("t", "time", &argparse.Options{Help: "Daily time limit in minutes", Default: 90})
	err := parser.Parse(os.Args)
	if err != nil {
		// In case of error print error and print usage
		// This can also be done by passing -h or --help flags
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}
	timeLimit = *tempTimeLimit
	add := firewalldCycle{add: true}
	addCommands = append(addCommands, insertUsernameChain(*userName, firewalldCommandStringChainArguments, add))
	addCommands = append(addCommands, insertUsernameOwner(*userName, firewalldCommandStringOwnerArguments, add))
	addCommands = append(addCommands, insertUsernameDrop(*userName, firewalldCommandStringDropArguments, add))
	addCommands = append(addCommands, firewalldReloadCommand)
	remove := firewalldCycle{remove: true}
	removeCommands = append(removeCommands, insertUsernameChain(*userName, firewalldCommandStringChainArguments, remove))
	removeCommands = append(removeCommands, insertUsernameOwner(*userName, firewalldCommandStringOwnerArguments, remove))
	removeCommands = append(removeCommands, insertUsernameDrop(*userName, firewalldCommandStringDropArguments, remove))
	removeCommands = append(removeCommands, firewalldReloadCommand)
}

func executeAddCommands() {
	log.Info("Adding firewalld rules")
	for _, addCommand := range addCommands {
		command := exec.Command(sudoCommandString, addCommand...)
		var commandOutput bytes.Buffer
		command.Stdout = &commandOutput
		err := command.Run()
		if err != nil {
			log.Error(err)
			os.Exit(1)
		}

	}

}

func executeRemoveCommands() {
	log.Info("Removing firewalld rules")
	for _, removeCommand := range removeCommands {
		command := exec.Command(sudoCommandString, removeCommand...)
		var commandOutput bytes.Buffer
		command.Stdout = &commandOutput
		err := command.Run()
		if err != nil {
			log.Error(err)
		}
	}

}

func loadDataFile() {
	viper.SetConfigName("data") // name of config file (without extension)
	viper.SetConfigType("yaml") // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(dataFolder)
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Error("File not found data.yml:", err)
		} else {
			log.Error("Error reading data.yml: ", err)
		}
	}
}

func createDataFile() {

	if _, err := os.Stat(dataFolder); os.IsNotExist(err) {
		err := os.Mkdir(dataFolder, 0755)
		if err != nil {
			log.Error("Failed to create data folder: ", err)
			os.Exit(1)
		}
	}

	if _, err := os.Stat(dataFolder + "data.yml"); os.IsNotExist(err) {
		// currentTime := time.Now()
		// viper.Set("dayDate", currentTime.Format("02-01-2006"))
		err := viper.WriteConfigAs(dataFolder + "data.yml")
		if err != nil {
			log.Error("Error wrting data.yml: ", err)

		}

	}
}

func writeDataFile() {
	err := viper.WriteConfigAs(dataFolder + "data.yml")
	if err != nil {
		log.Error("Error wrting data.yml: ", err)

	}
}

func run() {
	currentTime := time.Now()
	currentTimeString := currentTime.Format("02-01-2006")
	nextMidnightTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), 24, 0, 0, 0, currentTime.Location())
	untilMidnightDuration := nextMidnightTime.Sub(currentTime)
	if untilMidnightDuration < 0 {
		nextMidnightTime = nextMidnightTime.Add(24 * time.Hour)
		untilMidnightDuration = nextMidnightTime.Sub(currentTime)
	}
	for {
		tempDay := viper.Get("dayDate")
		if tempDay != "" || tempDay != currentTimeString {
			viper.Set("dayDate", currentTimeString)
			writeDataFile()
			go runAllowedTime() // need to also consider when user logs out
			time.Sleep(untilMidnightDuration)
			untilMidnightDuration = 24 * time.Hour

		}

	}

}

func runAllowedTime() {
	executeRemoveCommands()
	time.Sleep(time.Minute * 90)
	executeAddCommands()

}

func start() {
	log.Info("Starting...")
	options()
	createDataFile()
	loadDataFile()
	run()
	os.Exit(1)
	executeRemoveCommands() // Clean for bad exits
	log.Info("Time limit in minutes: ", strconv.Itoa(timeLimit))
	executeAddCommands()
}

func stop() {
	log.Info("Stopping...")
	executeRemoveCommands()

}

func main() {
	go start()
	sig := exit.Wait()
	log.Info("Signal recieved: ", sig)
	stop()

}
